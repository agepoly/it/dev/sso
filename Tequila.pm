package Lemonldap::NG::Portal::Auth::Tequila;

use strict;
use Mouse;
use URI::Escape;
use HTTP::Request;
use Lemonldap::NG::Common::UserAgent;
use Lemonldap::NG::Common::FormEncode;
use Lemonldap::NG::Portal::Main::Constants qw(
  PE_ERROR
  PE_IDPCHOICE
  PE_OK
  PE_REDIRECT
  PE_SENDRESPONSE
);

our $VERSION = '2.0.6';

extends 'Lemonldap::NG::Portal::Main::Auth';

has ua => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        return Lemonldap::NG::Common::UserAgent->new( $_[0]->{conf} );
    }
);

# INITIALIZATION

sub init {
    my ($self) = @_;
    $self->logger->debug("TEQUILA: initialization OK");
    return 1;
}

# RUNNING METHODS

sub extractFormInfo {
    my ( $self, $req ) = @_;

    my $key = $req->param('key');
    if(!$key){
      $self->logger->debug("TEQUILA: no tequila in the request, skipping key verification");
    } else {
      my $uniqueid = $self->verifKey($key);
      if($uniqueid) {
        $req->{user} = $uniqueid;
        return PE_OK;
      }
    }

  

    my $requestkey = $self->requestKey();
    return PE_ERROR unless $requestkey;
    
    
    my $url = "https://tequila.epfl.ch/tequila/requestauth?requestkey=".$requestkey;
    $self->logger->debug("TEQUILA: redirecting to $url");
    $req->{urldc} = $url;

    return PE_REDIRECT;
}

sub authenticate {
    return PE_OK;
}

# Set authenticationLevel.
sub setAuthSessionInfo {
    my ( $self, $req ) = @_;
    $req->{sessionInfo}->{authenticationLevel} = 2;
    return PE_OK;
}

sub authLogout {
    my ( $self, $req ) = @_;


    return PE_OK;
}

sub getDisplayType {
    return "logo";
}

sub requestKey {
  my ($self) = @_;
  my $url  = "https://tequila.epfl.ch/cgi-bin/tequila/createrequest";
  my $allows = "categorie=SHIBBOLETH";
  my $service = "SSO AGEPoly";
  my $urlaccess = $self->conf->{portal};
  my $request = "name,uniqueid,allunits";
  
  my $content = "allows=$allows\nservice=$service\nurlaccess=$urlaccess\nrequest=$request\n";
  my $header = ['Content-Type' => 'text/plain'];

  $self->logger->info("TEQUILA: requestkey url : $url");
  my $req = HTTP::Request->new( 'POST', $url, $header, $content);
  my $resp = $self->ua->request( $req );

  unless ($resp->is_success) {
    my $status = $resp->status_line;
    $self->logger->error("TEQUILA: requestkey error : $status $resp");
    return 0;
  };

  $_= $resp->content;
  my $requestkey = $1;
  unless (/^key=(.*)$/) {
    $self->logger->error("TEQUILA: requestkey error, cannot find key : $resp->content");
    return 0;
  }

  return $1;
}

sub verifKey {
  my ( $self, $key ) = @_;
  my $url  = "https://tequila.epfl.ch/cgi-bin/tequila/fetchattributes?key=".$key;
  $self->logger->info("TEQUILA: fetchattributes url : $url");
  my $req = HTTP::Request->new( 'GET', $url );
  my $resp = $self->ua->request( $req );
  unless ($resp->is_success) {
    my $status = $resp->status_line;
    $self->logger->error("TEQUILA: fetchattributes error : $status $resp");
    return 0;
  }

  my %attrs;
  my $uniqueid = 0;
  my $content = $resp->content;
  my @lines = split("\n",$content);
  foreach (@lines) {
    chomp;   
    next if /^$/;
    if (/^([^=]*)=(.*)$/) {
      my $name = $1;
      my $value = $2;
      if ($name eq 'uniqueid') { $uniqueid = $value; }
      else { $attrs {$name} = $value; }
    }
  }
  $self->userLogger->info("TEQUILA: tequila authenticated user : $url");
  return $uniqueid;
}

1;
