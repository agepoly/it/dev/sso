FROM debian:buster AS base
MAINTAINER Téo Goddet
LABEL name="lemonldap-ng-nginx-builder" \
      version="v2.0"

ENV SSODOMAIN=example.com \
    LOGLEVEL=info \
    DEBIAN_FRONTEND=noninteractive

EXPOSE 80

RUN echo "# Install LemonLDAP::NG source repo" && \
    apt-get -y update && \
    apt-get -y install wget apt-transport-https gnupg dumb-init && \
    wget -O - https://lemonldap-ng.org/_media/rpm-gpg-key-ow2 | apt-key add - && \
    echo "deb https://lemonldap-ng.org/deb 2.0 main" >/etc/apt/sources.list.d/lemonldap-ng.list

RUN apt-get -y update && \
    echo "# Install LemonLDAP::NG packages" && \
    apt-get -y install nginx lemonldap-ng cron anacron liblasso-perl libio-string-perl && \
    echo "# Install LemonLDAP::NG TOTP requirements" && \
    apt-get -y install libconvert-base32-perl libdigest-hmac-perl && \
    echo "# Install some DB drivers" && \
    apt-get -y install libdbd-mysql-perl libdbd-pg-perl && \
    echo "\ndaemon off;" >> /etc/nginx/nginx.conf

COPY docker-entrypoint.sh /

RUN echo '# Back up orignal configuration' && \
    cp -a /etc/lemonldap-ng /etc/lemonldap-ng-orig && \
    cp -a /var/lib/lemonldap-ng/conf /var/lib/lemonldap-ng/conf-orig && \
    cp -a /var/lib/lemonldap-ng/sessions /var/lib/lemonldap-ng/sessions-orig && \
    cp -a /var/lib/lemonldap-ng/psessions /var/lib/lemonldap-ng/psessions-orig

RUN echo "# Configure nginx to log to standard streams" && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

VOLUME ["/etc/lemonldap-ng","/var/lib/lemonldap-ng/conf", "/var/lib/lemonldap-ng/sessions", "/var/lib/lemonldap-ng/psessions"]

ENTRYPOINT ["dumb-init","--","/bin/sh", "/docker-entrypoint.sh"]

FROM base

# we need a specific handler to avoid some shit in the NGIX one
COPY Traefik.pm /usr/share/perl5/Lemonldap/NG/Handler/Server/Traefik.pm
RUN sed -i s/Lemonldap::NG::Handler::Server::Nginx/Lemonldap::NG::Handler::Server::Traefik/ /usr/sbin/llng-fastcgi-server

RUN rm /etc/nginx/sites-enabled/default
COPY portal-nginx.conf /etc/nginx/sites-enabled/portal.conf
COPY manager-nginx.conf /etc/nginx/sites-enabled/manager.conf
COPY traefik-handler-nginx.conf /etc/nginx/sites-enabled/traefik-handler.conf
COPY api-nginx.conf /etc/nginx/sites-enabled/api.conf

COPY Tequila.pm /usr/share/perl5/Lemonldap/NG/Portal/Auth/Tequila.pm

COPY Tequila.png /usr/share/lemonldap-ng/portal/htdocs/static/common/modules/Tequila.png